on va installer dedans mkdocs

pip install mkdocs 

Si ça ne marche pas, il peut y avoir besoin au préalable de créer un virtual env dans le dossier

virtualenv env

source env/bin/activate

si on fait mkdocs new www.tool cela crée un dossier www.tools avec à l'interieurun dossier "docs" contenant un fichier index.md et un fichier "mkdocs.yml"

|_www.tool
|_docs
|    |_index.md
|_mkdocs.yml

mkdocs [OPTION] - h

Cela nous donne de l'aide pour les 4 options de commande

création d'un dossier theme

dedans, création de  main.html

Dans lequel on va integrer le contenu du .yaml

syntaxe pour récupérer des éléments:

{{config.site_name}}

config = appel du fichier de configuration yaml à la racine du dossier (mkdocs.yml)

pour faire une boucle

exemple appeler les auteur·ices du yaml

<ul>
	{% for author in config.authors %}
		<li>{{ author }}</li>
	{% endfor %}
</ul>

pour réactualiser le thème et voir le résultat, appliquer la commande suivante dans le terminal

mkdocs serve --watch-theme

Ça c'est pour le site global.

Passons maintenant aux fichiers markdown individuels

dans le dossier doscs, un nouvel article

---
title: mon premier article
color: yellow

---

Dans le main html on peut faire ressortir plusieurs choses

{{ page.title}} 
{{ nav }}
etc.

Toute la doc se trouve ici

https://www.mkdocs.org/user-guide/

en php il y a beaucoup de sinclude, mais en jinja c'est des extend

on crée un nvx fichier qui va s'appeler base.html qui va aller dans le dossier theme dans lequel on va mettre ce qui est récurrent sur les sites (head, nav, footer etc.)

sur chaque fichier template,  on va ajouter la base

{% extends 'base.html' %}

À l'interieur du fichier base on intègre dans le body

{% block main %}
{% endblock % } 

Affiche le contenu du fichier .md

{{ page.content }}


------
J'ai ajouté différents pluggins qu'on peut installer :

pour les notes de bas de page:

pip install mkdocs-material

pour l'extension bibtex: 

pip install mkdocs-bibtex

pour les autoliens :

pip install mkdocs-autolinks-plugin

pour le glossaire : 

pip install mkdocs-glossary-plugin