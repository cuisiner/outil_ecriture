---
title: réunir
author: alice
tags:
    - fruit
    - pomme
---

> Convoquez les copaines des autres mondes, organisez des banquets où se réunir. 

> Ne fermez pas les écoutilles.

> Même quand tout ce monde prend l'eau.

> Laissez infuser davantage.[@suchet_proposition_2024]

cette citation est en relation à d'autres choses [pommes](../pomme)
